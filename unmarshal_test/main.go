package main

import (
	"bytes"
	"fmt"
	"github.com/golang/protobuf/jsonpb"
	"gitlab.com/YakovLachin/dgraphapp/service"
)

func main() {
	in := `{"tree":[{"uid":"0x2","name":"tree-001","expression":"d.age \u003e 21","positive":{"uid":"0x3","name":"result","expression":"true"},"negative":{"uid":"0x4","name":"sex","expression":"d.mail == true","positive":{"uid":"0x5","name":"age","expression":"b.age \u003e 18","positive":{"uid":"0x6","name":"result","expression":"true"},"negative":{"uid":"0x7","name":"result","expression":"true"}}}}]}`

	tree := &service.GetTreeResponse{}
	marshaler := jsonpb.Unmarshaler{
		AllowUnknownFields: true,
	}

	buf := bytes.NewBuffer(([]byte)(in))
	err := marshaler.Unmarshal(buf, tree)
	if err != nil {
		fmt.Println(err.Error())
	}
}

IMAGE="gitlab.com/yakovlachin/dgraphapp"
CWD=/app

image:
	@ docker build --force-rm -t $(IMAGE) . && \
	docker image prune -f --filter label=decision=builder

create-network:
	@-docker network create dev-network

up: create-network
	@ IMG=$(IMAGE) docker-compose up -d

build-setup:
	@docker build -t $(IMAGE)/setup ./setup

setup:
	@docker run  --network dev-network --env-file envfile --name=setup --rm $(IMAGE)/setup

service:
	@docker run --rm -it -v $(CURDIR):$(CURDIR) -w $(CURDIR) \
	grpc/go /bin/bash -c 'mkdir service && protoc --proto_path=./proto/ --go_out=plugins=grpc:./service service.proto'

clean:
	@-docker run --rm -v $(CURDIR):$(CWD) -w $(CWD) golang:1.15-alpine sh -c "rm -rf ./tmp/*"

down:
	@- IMG=$(IMAGE) docker-compose down --rmi local

.PHONY: image setup build-setup

.DEFAULT_GOAL: image
FROM golang:1.15-alpine as builder
LABEL decision=builder
WORKDIR /go/src/gitlab.com/YakovLachin/dgraphapp
COPY . .
RUN GOOS=linux GOARCH=amd64 go build -v -o rel/app

FROM golang:1.15-alpine
WORKDIR /app
#EXPOSE 8080
#EXPOSE 8090
COPY --from=builder /go/src/gitlab.com/YakovLachin/dgraphapp ./
CMD /app/rel/app
package config

import "fmt"

type DGraphConfig struct {
	host string
	port string
}

func getDgraphConfig() (*DGraphConfig, error ){
	host, err := GetConfigValue("DGRAPH_HOST")
	if err != nil {
		return nil, err
	}

	port, err := GetConfigValue("DGRAPH_PORT")
	if err != nil {
		return nil, err
	}

	return &DGraphConfig{
		host: host,
		port: port,
	}, nil
}

func DGraphGRPCTargetFromConfig() (string, error){
	conf, err := getDgraphConfig()
	if err != nil {
		return  "",  fmt.Errorf("FAIL GET DGRAPH CONFIG TARGET: %s", err.Error())
	}

	return  conf.host + ":" + conf.port, nil
}

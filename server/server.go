package server

import (
	"github.com/dgraph-io/dgo/v200"
	"github.com/sirupsen/logrus"
	"gitlab.com/YakovLachin/dgraphapp/service"
)

type srv struct {
	Dgraph *dgo.Dgraph
	logger *logrus.Entry
}

type Server interface {
	service.DecisionTreeServer
	service.TreeDecisionServer
	service.ChatServer
}

func NewSrv(dgraph *dgo.Dgraph, logger *logrus.Entry) Server {
	return &srv{
		Dgraph: dgraph,
		logger: logger,
	}
}

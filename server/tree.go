package server

import (
	"context"
	"fmt"

	"gitlab.com/YakovLachin/dgraphapp/dgraph"
	"gitlab.com/YakovLachin/dgraphapp/service"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *srv) GetDecisionTree(ctx context.Context, req *service.TreeRequest) (*service.TreeResponse, error) {
	tr := req.GetTree()
	if tr.GetName() == "" {
		return nil, status.Error(codes.InvalidArgument,  "Fail GetDecisionTree: name is required")
	}

	txn := s.Dgraph.NewTxn()
	res, err := dgraph.GetNode(txn, ctx, tr)
	if res == nil && err == nil {
		return nil, status.Error(codes.NotFound,  fmt.Sprintf("Fail GetDesicionTree: not found"))
	}

	if err != nil {
		return nil, status.Error(codes.Internal,  fmt.Sprintf("Fail GetDecisionTree: %s", err.Error()))
	}

	return &service.TreeResponse{
		Tree: res,
	}, nil
}



package server

import (
	"context"
	"fmt"

	"gitlab.com/YakovLachin/dgraphapp/decision"
	"gitlab.com/YakovLachin/dgraphapp/dgraph"
	"gitlab.com/YakovLachin/dgraphapp/service"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

)

func (s *srv) PushButton(ctx context.Context, req *service.PushButtonRequest) (*service.ChatResponse, error) {
	uid := req.GetUID()
	if uid == "" {
		return nil, status.Error(codes.InvalidArgument,  "Fail GetDecisionTree: name is required")
	}

	txn := s.Dgraph.NewTxn()
	tree, err := dgraph.GetNodeByUID(txn, ctx, uid)
	if tree == nil && err == nil {
		return nil, status.Error(codes.NotFound, "Not FOUND")
	}

	if err != nil {
		return nil, status.Error(codes.Internal,  fmt.Sprintf("Internal Fail get From dgraph: %s", err.Error()))
	}

	dec, err := decision.GetChatResponseByTreeAndData(tree, req.Facts)
	if err != nil {
		return nil, status.Error(codes.Internal,  fmt.Sprintf("Fail IntrepretDesicionTree: %s", err.Error()))
	}

	return dec, nil
}
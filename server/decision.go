package server

import (
	"context"
	"fmt"

	"gitlab.com/YakovLachin/dgraphapp/decision"
	"gitlab.com/YakovLachin/dgraphapp/dgraph"
	"gitlab.com/YakovLachin/dgraphapp/service"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

)

func (s *srv) GetDecision(ctx context.Context, req *service.DecisionRequest) (*service.DecisionResponse, error) {
	tr := req.GetTree()
	if tr.GetName() == "" {
		return nil, status.Error(codes.InvalidArgument,  "Fail GetDecisionTree: name is required")
	}

	txn := s.Dgraph.NewTxn()
	tree, err := dgraph.GetNode(txn, ctx, tr)
	if tree == nil && err == nil {
		return nil, status.Error(codes.NotFound, "Not FOUND")
	}

	if err != nil {
		return nil, status.Error(codes.Internal,  fmt.Sprintf("Internal Fail get From dgraph: %s", err.Error()))
	}

	dec, err := decision.GetDecisionByTreeAndData(tree, req.Facts)
	if err != nil {
		return nil, status.Error(codes.Internal,  fmt.Sprintf("Fail IntrepretDesicionTree: %s", err.Error()))
	}

	return &service.DecisionResponse{Decision: dec}, nil
}

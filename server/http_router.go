package server

import (
	"net/http"
)

func NewMux(grpcSrv Server) *http.ServeMux {
    adapter := newHttpAdapter(grpcSrv)
	mux := http.NewServeMux()
	mux.HandleFunc("/tree/get", adapter.GetTree)
	mux.HandleFunc("/decision", adapter.GetDecision)
	mux.HandleFunc("/button/push", adapter.PushButton)

	return mux
}

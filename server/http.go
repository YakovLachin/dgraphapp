package server

import (
	"context"
	"io/ioutil"
	"net/http"

	"gitlab.com/YakovLachin/dgraphapp/service"

	"github.com/golang/protobuf/jsonpb"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type HttpServerAdapter struct {
	GrpcSrv     Server
}


func newHttpAdapter(grpcSrv Server) *HttpServerAdapter {
	return &HttpServerAdapter{
		GrpcSrv: grpcSrv,
	}
}

func (srv *HttpServerAdapter) GetTree(writer http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))

		return
	}

	grpcReq	:= service.TreeRequest{}
	err = jsonpb.UnmarshalString(string(body), &grpcReq)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL UNMARSHALING  REQUEST" + err.Error()))
		return
	}

	ctx := context.Background()
	resp, err:= srv.GrpcSrv.GetDecisionTree(ctx, &grpcReq)
	if c, ok := status.FromError(err); !ok || c.Code() == codes.Internal {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL GET TREE:" + c.Message()))
		return
	} else if c.Code() == codes.AlreadyExists {
		writer.WriteHeader(http.StatusConflict)
		writer.Write([]byte("FAIL GET TREE:" + c.Message()))
		return
	} else if c.Code() == codes.InvalidArgument {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET TREE:" + err.Error()))
		return
	}

	marshaler := jsonpb.Marshaler{}
	res, err := marshaler.MarshalToString(resp)

	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL GET TREE Marshling: %s" + err.Error()))
		return
	}

	writer.WriteHeader(http.StatusCreated)
	writer.Write([]byte(res))
}

func (srv *HttpServerAdapter) GetDecision(writer http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))

		return
	}

	grpcReq	:= service.DecisionRequest{}
	err = jsonpb.UnmarshalString(string(body), &grpcReq)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL UNMARSHALING HR :" + err.Error()))
		return
	}

	ctx := context.Background()
	resp, err:= srv.GrpcSrv.GetDecision(ctx, &grpcReq)
	if c, ok := status.FromError(err); !ok || c.Code() == codes.Internal {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL GET TREE:" + c.Message()))
		return
	} else if c.Code() == codes.AlreadyExists {
		writer.WriteHeader(http.StatusConflict)
		writer.Write([]byte("FAIL GET TREE:" + c.Message()))
		return
	} else if c.Code() == codes.InvalidArgument {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET TREE:" + err.Error()))
		return
	}

	marshaler :=  &runtime.JSONPb{OrigName: true, EmitDefaults: true}
	res, err := marshaler.Marshal(resp)

	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL GET DESICION Marshling: %s" + err.Error()))
		return
	}

	writer.WriteHeader(http.StatusCreated)
	writer.Write([]byte(res))
}

func (srv *HttpServerAdapter) PushButton(writer http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET BODY:" + err.Error()))

		return
	}

	grpcReq	:= service.PushButtonRequest{}
	err = jsonpb.UnmarshalString(string(body), &grpcReq)

	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL UNMARSHALING HR :" + err.Error()))
		return
	}

	ctx := context.Background()
	resp, err:= srv.GrpcSrv.PushButton(ctx, &grpcReq)
	if c, ok := status.FromError(err); !ok || c.Code() == codes.Internal {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL GET TREE:" + c.Message()))
		return
	} else if c.Code() == codes.AlreadyExists {
		writer.WriteHeader(http.StatusConflict)
		writer.Write([]byte("FAIL GET TREE:" + c.Message()))
		return
	} else if c.Code() == codes.InvalidArgument {
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("FAIL GET TREE:" + err.Error()))
		return
	}

	marshaler :=  &runtime.JSONPb{OrigName: true, EmitDefaults: false}
	res, err := marshaler.Marshal(resp)

	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("FAIL GET DESICION Marshling: %s" + err.Error()))
		return
	}

	writer.WriteHeader(http.StatusCreated)
	writer.Write(res)

}
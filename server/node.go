package server

import (
	"context"

	"gitlab.com/YakovLachin/dgraphapp/service"
)

func (s *srv) GetNode(context.Context, *service.NodeRequest) (*service.NodeRequest, error) {
	panic("implement me")
}

func (s *srv) CreateNode(context.Context, *service.NodeRequest) (*service.NodeResponse, error) {
	panic("implement me")
}

func (s *srv) UpdateNode(context.Context, *service.NodeRequest) (*service.NodeResponse, error) {
	panic("implement me")
}

func (s *srv) DeleteNode(context.Context, *service.NodeRequest) (*service.NodeResponse, error) {
	panic("implement me")
}

package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.com/YakovLachin/dgraphapp/config"
	"gitlab.com/YakovLachin/dgraphapp/dgraph"
	"gitlab.com/YakovLachin/dgraphapp/server"
	"gitlab.com/YakovLachin/dgraphapp/service"

	"github.com/sirupsen/logrus"

	"google.golang.org/grpc"
)

func main()  {
	log := logrus.New()
	log.Level = logrus.DebugLevel
	logger := log.WithField("chanel", "dgraph")
	logger.Info("Starting server")

	target, err := config.DGraphGRPCTargetFromConfig()
	if err != nil {
		fmt.Errorf("Fail start application")
		os.Exit(1)
	}

	cl := dgraph.NewGRPCClient(target)

	var opts []grpc.ServerOption
	grpcServ := grpc.NewServer(opts...)

	srv := server.NewSrv(cl, logger)
	service.RegisterDecisionTreeServer(grpcServ, srv)
	service.RegisterTreeDecisionServer(grpcServ, srv)
	service.RegisterChatServer(grpcServ, srv)

	httpSrv := &http.Server{
		Addr:         "0.0.0.0:8080",
		Handler:      server.NewMux(srv),
		ReadTimeout:  time.Duration(5 * time.Second),
		WriteTimeout: time.Duration(5 * time.Second),
	}

	sigchan := make(chan os.Signal)
	go httpSrv.ListenAndServe()
	<-sigchan
}

module gitlab.com/YakovLachin/dgraphapp/setup

go 1.15

require (
	github.com/dgraph-io/dgo v1.0.0
	github.com/dgraph-io/dgo/v200 v200.0.0-20210401091508-95bfd74de60e
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/YakovLachin/dgraphapp v0.0.0-20210416163043-105b26192ade
)

// Code generated by protoc-gen-go. DO NOT EDIT.
// source: service.proto

/*
Package service is a generated protocol buffer package.

It is generated from these files:
	service.proto

It has these top-level messages:
	TreeRequest
	TreeResponse
	NodeRequest
	NodeResponse
	Node
	GetTreeResponse
	DecisionRequest
	DecisionResponse
*/
package service

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import google_protobuf "github.com/golang/protobuf/ptypes/struct"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion2 // please upgrade the proto package

type TreeRequest struct {
	Tree *Node `protobuf:"bytes,1,opt,name=tree" json:"tree,omitempty"`
}

func (m *TreeRequest) Reset()                    { *m = TreeRequest{} }
func (m *TreeRequest) String() string            { return proto.CompactTextString(m) }
func (*TreeRequest) ProtoMessage()               {}
func (*TreeRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *TreeRequest) GetTree() *Node {
	if m != nil {
		return m.Tree
	}
	return nil
}

type TreeResponse struct {
	Tree *Node `protobuf:"bytes,1,opt,name=tree" json:"tree,omitempty"`
}

func (m *TreeResponse) Reset()                    { *m = TreeResponse{} }
func (m *TreeResponse) String() string            { return proto.CompactTextString(m) }
func (*TreeResponse) ProtoMessage()               {}
func (*TreeResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *TreeResponse) GetTree() *Node {
	if m != nil {
		return m.Tree
	}
	return nil
}

type NodeRequest struct {
	Node *Node `protobuf:"bytes,1,opt,name=node" json:"node,omitempty"`
}

func (m *NodeRequest) Reset()                    { *m = NodeRequest{} }
func (m *NodeRequest) String() string            { return proto.CompactTextString(m) }
func (*NodeRequest) ProtoMessage()               {}
func (*NodeRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *NodeRequest) GetNode() *Node {
	if m != nil {
		return m.Node
	}
	return nil
}

type NodeResponse struct {
	Node *Node `protobuf:"bytes,1,opt,name=node" json:"node,omitempty"`
}

func (m *NodeResponse) Reset()                    { *m = NodeResponse{} }
func (m *NodeResponse) String() string            { return proto.CompactTextString(m) }
func (*NodeResponse) ProtoMessage()               {}
func (*NodeResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{3} }

func (m *NodeResponse) GetNode() *Node {
	if m != nil {
		return m.Node
	}
	return nil
}

type Node struct {
	Uid        string  `protobuf:"bytes,1,opt,name=uid" json:"uid,omitempty"`
	Positive   []*Node `protobuf:"bytes,2,rep,name=positive" json:"positive,omitempty"`
	Negative   []*Node `protobuf:"bytes,3,rep,name=negative" json:"negative,omitempty"`
	Expression string  `protobuf:"bytes,4,opt,name=expression" json:"expression,omitempty"`
	Name       string  `protobuf:"bytes,5,opt,name=name" json:"name,omitempty"`
}

func (m *Node) Reset()                    { *m = Node{} }
func (m *Node) String() string            { return proto.CompactTextString(m) }
func (*Node) ProtoMessage()               {}
func (*Node) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{4} }

func (m *Node) GetUid() string {
	if m != nil {
		return m.Uid
	}
	return ""
}

func (m *Node) GetPositive() []*Node {
	if m != nil {
		return m.Positive
	}
	return nil
}

func (m *Node) GetNegative() []*Node {
	if m != nil {
		return m.Negative
	}
	return nil
}

func (m *Node) GetExpression() string {
	if m != nil {
		return m.Expression
	}
	return ""
}

func (m *Node) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

type GetTreeResponse struct {
	Tree []*Node `protobuf:"bytes,1,rep,name=tree" json:"tree,omitempty"`
}

func (m *GetTreeResponse) Reset()                    { *m = GetTreeResponse{} }
func (m *GetTreeResponse) String() string            { return proto.CompactTextString(m) }
func (*GetTreeResponse) ProtoMessage()               {}
func (*GetTreeResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{5} }

func (m *GetTreeResponse) GetTree() []*Node {
	if m != nil {
		return m.Tree
	}
	return nil
}

type DecisionRequest struct {
	Tree  *Node                   `protobuf:"bytes,1,opt,name=tree" json:"tree,omitempty"`
	Facts *google_protobuf.Struct `protobuf:"bytes,2,opt,name=facts" json:"facts,omitempty"`
}

func (m *DecisionRequest) Reset()                    { *m = DecisionRequest{} }
func (m *DecisionRequest) String() string            { return proto.CompactTextString(m) }
func (*DecisionRequest) ProtoMessage()               {}
func (*DecisionRequest) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{6} }

func (m *DecisionRequest) GetTree() *Node {
	if m != nil {
		return m.Tree
	}
	return nil
}

func (m *DecisionRequest) GetFacts() *google_protobuf.Struct {
	if m != nil {
		return m.Facts
	}
	return nil
}

type DecisionResponse struct {
	Decision bool `protobuf:"varint,1,opt,name=decision" json:"decision,omitempty"`
}

func (m *DecisionResponse) Reset()                    { *m = DecisionResponse{} }
func (m *DecisionResponse) String() string            { return proto.CompactTextString(m) }
func (*DecisionResponse) ProtoMessage()               {}
func (*DecisionResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{7} }

func (m *DecisionResponse) GetDecision() bool {
	if m != nil {
		return m.Decision
	}
	return false
}

func init() {
	proto.RegisterType((*TreeRequest)(nil), "TreeRequest")
	proto.RegisterType((*TreeResponse)(nil), "TreeResponse")
	proto.RegisterType((*NodeRequest)(nil), "NodeRequest")
	proto.RegisterType((*NodeResponse)(nil), "NodeResponse")
	proto.RegisterType((*Node)(nil), "Node")
	proto.RegisterType((*GetTreeResponse)(nil), "GetTreeResponse")
	proto.RegisterType((*DecisionRequest)(nil), "DecisionRequest")
	proto.RegisterType((*DecisionResponse)(nil), "DecisionResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for DicisionTree service

type DicisionTreeClient interface {
	GetDecisionTree(ctx context.Context, in *TreeRequest, opts ...grpc.CallOption) (*TreeResponse, error)
	GetNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeRequest, error)
	CreateNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeResponse, error)
	UpdateNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeResponse, error)
	DeleteNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeResponse, error)
}

type dicisionTreeClient struct {
	cc *grpc.ClientConn
}

func NewDicisionTreeClient(cc *grpc.ClientConn) DicisionTreeClient {
	return &dicisionTreeClient{cc}
}

func (c *dicisionTreeClient) GetDecisionTree(ctx context.Context, in *TreeRequest, opts ...grpc.CallOption) (*TreeResponse, error) {
	out := new(TreeResponse)
	err := grpc.Invoke(ctx, "/DicisionTree/getDecisionTree", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dicisionTreeClient) GetNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeRequest, error) {
	out := new(NodeRequest)
	err := grpc.Invoke(ctx, "/DicisionTree/getNode", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dicisionTreeClient) CreateNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeResponse, error) {
	out := new(NodeResponse)
	err := grpc.Invoke(ctx, "/DicisionTree/createNode", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dicisionTreeClient) UpdateNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeResponse, error) {
	out := new(NodeResponse)
	err := grpc.Invoke(ctx, "/DicisionTree/updateNode", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *dicisionTreeClient) DeleteNode(ctx context.Context, in *NodeRequest, opts ...grpc.CallOption) (*NodeResponse, error) {
	out := new(NodeResponse)
	err := grpc.Invoke(ctx, "/DicisionTree/deleteNode", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for DicisionTree service

type DicisionTreeServer interface {
	GetDecisionTree(context.Context, *TreeRequest) (*TreeResponse, error)
	GetNode(context.Context, *NodeRequest) (*NodeRequest, error)
	CreateNode(context.Context, *NodeRequest) (*NodeResponse, error)
	UpdateNode(context.Context, *NodeRequest) (*NodeResponse, error)
	DeleteNode(context.Context, *NodeRequest) (*NodeResponse, error)
}

func RegisterDicisionTreeServer(s *grpc.Server, srv DicisionTreeServer) {
	s.RegisterService(&_DicisionTree_serviceDesc, srv)
}

func _DicisionTree_GetDecisionTree_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(TreeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DicisionTreeServer).GetDecisionTree(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DicisionTree/GetDecisionTree",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DicisionTreeServer).GetDecisionTree(ctx, req.(*TreeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DicisionTree_GetNode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DicisionTreeServer).GetNode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DicisionTree/GetNode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DicisionTreeServer).GetNode(ctx, req.(*NodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DicisionTree_CreateNode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DicisionTreeServer).CreateNode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DicisionTree/CreateNode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DicisionTreeServer).CreateNode(ctx, req.(*NodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DicisionTree_UpdateNode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DicisionTreeServer).UpdateNode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DicisionTree/UpdateNode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DicisionTreeServer).UpdateNode(ctx, req.(*NodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _DicisionTree_DeleteNode_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NodeRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(DicisionTreeServer).DeleteNode(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/DicisionTree/DeleteNode",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(DicisionTreeServer).DeleteNode(ctx, req.(*NodeRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _DicisionTree_serviceDesc = grpc.ServiceDesc{
	ServiceName: "DicisionTree",
	HandlerType: (*DicisionTreeServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "getDecisionTree",
			Handler:    _DicisionTree_GetDecisionTree_Handler,
		},
		{
			MethodName: "getNode",
			Handler:    _DicisionTree_GetNode_Handler,
		},
		{
			MethodName: "createNode",
			Handler:    _DicisionTree_CreateNode_Handler,
		},
		{
			MethodName: "updateNode",
			Handler:    _DicisionTree_UpdateNode_Handler,
		},
		{
			MethodName: "deleteNode",
			Handler:    _DicisionTree_DeleteNode_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "service.proto",
}

// Client API for TreeDecision service

type TreeDecisionClient interface {
	GetDecision(ctx context.Context, in *DecisionRequest, opts ...grpc.CallOption) (*DecisionResponse, error)
}

type treeDecisionClient struct {
	cc *grpc.ClientConn
}

func NewTreeDecisionClient(cc *grpc.ClientConn) TreeDecisionClient {
	return &treeDecisionClient{cc}
}

func (c *treeDecisionClient) GetDecision(ctx context.Context, in *DecisionRequest, opts ...grpc.CallOption) (*DecisionResponse, error) {
	out := new(DecisionResponse)
	err := grpc.Invoke(ctx, "/TreeDecision/getDecision", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for TreeDecision service

type TreeDecisionServer interface {
	GetDecision(context.Context, *DecisionRequest) (*DecisionResponse, error)
}

func RegisterTreeDecisionServer(s *grpc.Server, srv TreeDecisionServer) {
	s.RegisterService(&_TreeDecision_serviceDesc, srv)
}

func _TreeDecision_GetDecision_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DecisionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(TreeDecisionServer).GetDecision(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/TreeDecision/GetDecision",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(TreeDecisionServer).GetDecision(ctx, req.(*DecisionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _TreeDecision_serviceDesc = grpc.ServiceDesc{
	ServiceName: "TreeDecision",
	HandlerType: (*TreeDecisionServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "getDecision",
			Handler:    _TreeDecision_GetDecision_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "service.proto",
}

func init() { proto.RegisterFile("service.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 381 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x93, 0xcf, 0x4e, 0xab, 0x40,
	0x14, 0xc6, 0x43, 0xa1, 0xb7, 0xed, 0x81, 0xa6, 0xbd, 0xb3, 0xb9, 0x5c, 0x62, 0x4c, 0x25, 0x31,
	0x69, 0x13, 0x9d, 0x26, 0xf8, 0x06, 0x4d, 0x13, 0x77, 0x2e, 0xd0, 0x95, 0xae, 0x28, 0x9c, 0x12,
	0x92, 0xca, 0x20, 0x33, 0x34, 0x3e, 0x87, 0x0f, 0xe9, 0x73, 0x18, 0x66, 0x4a, 0xf9, 0x13, 0xab,
	0xdd, 0xcd, 0xf9, 0xce, 0x8f, 0x0f, 0xce, 0xf9, 0x06, 0x18, 0x73, 0xcc, 0xf7, 0x49, 0x88, 0x34,
	0xcb, 0x99, 0x60, 0xce, 0x45, 0xcc, 0x58, 0xbc, 0xc3, 0xa5, 0xac, 0x36, 0xc5, 0x76, 0xc9, 0x45,
	0x5e, 0x84, 0x42, 0x75, 0xdd, 0x39, 0x98, 0x4f, 0x39, 0xa2, 0x8f, 0x6f, 0x05, 0x72, 0x41, 0xfe,
	0x83, 0x21, 0x72, 0x44, 0x5b, 0x9b, 0x69, 0x73, 0xd3, 0xeb, 0xd3, 0x07, 0x16, 0xa1, 0x2f, 0x25,
	0x77, 0x01, 0x96, 0x22, 0x79, 0xc6, 0x52, 0x8e, 0x3f, 0xa1, 0x73, 0x30, 0x65, 0x55, 0x9b, 0xa6,
	0x2c, 0xea, 0x92, 0xa5, 0x54, 0x9a, 0x2a, 0xb2, 0x36, 0x3d, 0x85, 0x7e, 0x68, 0x60, 0x94, 0x25,
	0x99, 0x82, 0x5e, 0x24, 0x91, 0x44, 0x46, 0x7e, 0x79, 0x24, 0x57, 0x30, 0xcc, 0x18, 0x4f, 0x44,
	0xb2, 0x47, 0xbb, 0x37, 0xd3, 0xeb, 0x27, 0x8f, 0x72, 0x89, 0xa4, 0x18, 0x07, 0x12, 0xd1, 0x5b,
	0x48, 0x25, 0x93, 0x4b, 0x00, 0x7c, 0xcf, 0x72, 0xe4, 0x3c, 0x61, 0xa9, 0x6d, 0x48, 0xfb, 0x86,
	0x42, 0x08, 0x18, 0x69, 0xf0, 0x8a, 0x76, 0x5f, 0x76, 0xe4, 0xd9, 0xbd, 0x81, 0xc9, 0x3d, 0x8a,
	0x13, 0x7b, 0xd1, 0xbb, 0x7b, 0x79, 0x81, 0xc9, 0x1a, 0xc3, 0xa4, 0x74, 0xfb, 0x7d, 0xe1, 0xe4,
	0x16, 0xfa, 0xdb, 0x20, 0x14, 0xdc, 0xee, 0xc9, 0xde, 0x3f, 0xaa, 0x82, 0xa4, 0x55, 0x90, 0xf4,
	0x51, 0x06, 0xe9, 0x2b, 0xca, 0xa5, 0x30, 0xad, 0xcd, 0x0f, 0xdf, 0xe2, 0xc0, 0x30, 0x3a, 0x68,
	0xf2, 0x0d, 0x43, 0xff, 0x58, 0x7b, 0x9f, 0x1a, 0x58, 0xeb, 0x44, 0x15, 0xe5, 0x00, 0x84, 0xc2,
	0x24, 0x46, 0x51, 0x79, 0x48, 0xc9, 0xa2, 0x8d, 0xcb, 0xe1, 0x8c, 0x69, 0x6b, 0xd0, 0x6b, 0x18,
	0xc4, 0x28, 0x64, 0x24, 0x16, 0x6d, 0xe4, 0xed, 0xb4, 0x2a, 0xb2, 0x00, 0x08, 0x73, 0x0c, 0x04,
	0x7e, 0x43, 0x8e, 0x69, 0x2b, 0xfd, 0x05, 0x40, 0x91, 0x45, 0xe7, 0xa2, 0x11, 0xee, 0xf0, 0x0c,
	0xd4, 0x5b, 0xa9, 0x8b, 0x5b, 0x0d, 0x46, 0x3c, 0x30, 0x1b, 0x73, 0x92, 0x29, 0xed, 0x64, 0xe2,
	0xfc, 0xa5, 0xdd, 0x45, 0xae, 0x46, 0xcf, 0x83, 0xc3, 0x5f, 0xb5, 0xf9, 0x23, 0xf7, 0x7f, 0xf7,
	0x15, 0x00, 0x00, 0xff, 0xff, 0x9f, 0x66, 0x3b, 0x2b, 0x67, 0x03, 0x00, 0x00,
}

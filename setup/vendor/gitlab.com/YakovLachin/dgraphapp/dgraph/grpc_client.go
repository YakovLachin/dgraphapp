package dgraph

import (
	"github.com/dgraph-io/dgo/v200"
	"github.com/dgraph-io/dgo/v200/protos/api"
	"google.golang.org/grpc"
)

func NewGRPCClient(target string) *dgo.Dgraph {
	conn, err := grpc.Dial(target, grpc.WithInsecure())
	if err != nil {

	}

	return dgo.NewDgraphClient(api.NewDgraphClient(conn))
}

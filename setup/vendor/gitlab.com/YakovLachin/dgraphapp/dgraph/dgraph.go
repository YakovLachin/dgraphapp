package dgraph

import (
	"bytes"
	"github.com/dgraph-io/dgo/v200"
	"gitlab.com/YakovLachin/dgraphapp/service"
	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"
	"fmt"
)

func GetNode(tx *dgo.Txn, ctx context.Context, req *service.Node) (tree *service.Node, err error) {
	q := `{
		   tree (func: allofterms(name, "%s")) @recurse(depth: 5, loop: true) {
			 uid
		     name
		     expression
		     positive
		     negative
		   }
		 }`

    query := fmt.Sprintf(q, req.GetName())
	res, err := tx.Query(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("fail get from dgraph %s. query: %s", err , query)
	}

	resp := &service.GetTreeResponse{
		Tree: []*service.Node{},
	}

	fmt.Println(string(res.GetJson()))
	reader := bytes.NewReader(res.GetJson())
	err = jsonpb.Unmarshal(reader, resp)
	if err != nil {
		return  nil, fmt.Errorf("fail unmarshaling %s", err)
	}

	if len(resp.Tree) == 0 {
		return nil, nil
	}
	tree = resp.Tree[0]

	return tree, nil
}

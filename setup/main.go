package main

import (
	"gitlab.com/YakovLachin/dgraphapp/setup/helper"
	"os"

	"gitlab.com/YakovLachin/dgraphapp/config"
	"gitlab.com/YakovLachin/dgraphapp/dgraph"

	"github.com/sirupsen/logrus"
)

func main() {
	log := logrus.New()
	log.Level = logrus.DebugLevel
	logger := log.WithField("chanel", "dgraph")
	logger.Info("Starting setup")

	t, err := config.DGraphGRPCTargetFromConfig()
	if err != nil {
		logger.Error("Fail get config")
		os.Exit(1)
	}

	cl := dgraph.NewGRPCClient(t)
	err = setupSchema(cl)
	if err != nil {
		logger.Errorf("Fail set Schema : %s", err.Error())
		os.Exit(1)
	}
	jsns, err := helper.GetJSONs("./fixtures")
	if err != nil {
		logger.Errorf("error to get files: %s", err.Error())
		os.Exit(1)
	}

	err = setNodes(cl, jsns, logger)
	if err != nil {
		logger.Errorf("Fail set Nodes: %s", err.Error())
		os.Exit(1)
	}
}

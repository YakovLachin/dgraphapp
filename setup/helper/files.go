package helper

import (
	"encoding/json"

	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func GetJSONs(dir string) ([][]byte, error) {
	if !DirIsExists(dir) {
		return nil, fmt.Errorf("dir %s is empty", dir)
	}

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	res := make([][]byte, 0, len(files))
	for _, f := range files {
		f, err := os.Open(dir +"/"+f.Name())
		if err != nil {
			return nil, fmt.Errorf("fail to open file %s", err.Error())
		}

		jsn, err := ioutil.ReadAll(f)
		if err != nil {
			return nil, fmt.Errorf("fail to read file %s. %s", f.Name(), err.Error())
		}

		if len(jsn) == 0 {
			return nil, fmt.Errorf("file %s is empty", f.Name())
		}

		if !json.Valid(jsn) {
			return nil, fmt.Errorf("file %s is invalid", f.Name())
		}

		res = append(res, jsn)
	}

	return res, nil
}

func DirIsExists(dirname string) bool {
	info, err := os.Stat(dirname)
	if os.IsNotExist(err) {

		return false
	}

	return info.IsDir()
}

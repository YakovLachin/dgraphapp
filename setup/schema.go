package main

import (
	"context"

	"github.com/dgraph-io/dgo/v200"
	"github.com/dgraph-io/dgo/v200/protos/api"
)

var schema = `
			name: string @index(term) .
			expression: string .
			description: string .
			negative: uid @count .
			positive: uid @count .
			alternative: uid @count .
			next: uid @count .
			data: [uid] @count .
			type: string .
`

func setupSchema(cl *dgo.Dgraph) error {
	ctx := context.Background()
	err := cl.Alter(ctx, &api.Operation{
		Schema: schema,
	})

	return err
}

package main

import (
	"context"
	"github.com/sirupsen/logrus"

	"github.com/dgraph-io/dgo/v200"
	"github.com/dgraph-io/dgo/v200/protos/api"
)

//var mutation string = `
//{
//	"name":"tree-001",
//	"expression": "d.age > 21",
//	"positive": {
//		"name" : "result",
//		"expression" : "true"
//	},
//	"negative" : {
//		"name" : "sex",
//		"expression" : "d.mail == true",
//		"positive" : {
//			"name" : "age",
//			"expression" : "d.age > 18",
//			"positive" : {
//				"name" : "result",
//				"expression" : "true"
//			},
//			"negative" : {
//				"name" : "result",
//				"expression" : "false"
//			}
//		},
//		"negative" : {
//			"name" : "result",
//			"expression" : "false"
//		}
//	}
//}
//`


var dgraphMutation string = `
{
  set {
    _:node1 <name> "tree-003" .
    _:node1 <expression> "d.age > 21" .
    _:node1 <negative> _:node2 .
    _:node1 <positive> _:node4 .

    _:node2 <name> "sex" .
    _:node2 <expression> "d.male == true" .
    _:node2 <positive> _:node3 .
	_:node2 <negative> _:node5 .

    _:node3 <name> "age" .
    _:node3 <expression> "d.age > 18" .
    _:node3 <positive> _:node6 .
    _:node3 <negative> _:node7 .

    _:node4 <name> "result" .
    _:node4 <expression> "true" .

	_:node5 <name> "result" .
    _:node5 <expression> "false" .

	_:node6 <name> "result" .
    _:node6 <expression> "true" .

	_:node7 <name> "result" .
    _:node7 <expression> "false" .
  }
}
`
func setNodes(cl *dgo.Dgraph, mutations [][]byte, logger *logrus.Entry) error {
	ctx := context.Background()
	txn := cl.NewTxn()

	for _, mutation := range mutations {
		logger.Info(string(mutation))
		_, err := txn.Mutate(
			ctx,
			&api.Mutation{
				SetJson: mutation,
			},
		)
		if err != nil {
			return err
		}
	}

	return txn.Commit(ctx)
}
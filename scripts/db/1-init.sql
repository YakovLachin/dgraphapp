CREATE DATABASE IF NOT EXISTS `chateditor`
    COLLATE 'utf8_general_ci'
    DEFAULT CHARSET 'utf8';

USE chateditor;

CREATE TABLE IF NOT EXISTS dictionary (
    `id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный id словаря',
    `intent_id` BIGINT(20) COMMENT 'Уникальный id намерения',
    `text`  TEXT NOT NULL DEFAULT '',
    `is_deleted` TINYINT(1) DEFAULT 0
);

CREATE TABLE IF NOT EXISTS `intents` (
    `id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный id намерения',
    `group_id` BIGINT(20) COMMENT 'Уникальный id группы намерения',
    `name` VARCHAR(255) NOT NULL DEFAULT '',
    `is_deleted` TINYINT(1) DEFAULT 0
);

CREATE TABLE IF NOT EXISTS versions(
    `id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY COMMENT 'Уникальный id намерения',
    `created_at` DATETIME DEFAULT NOW()
);

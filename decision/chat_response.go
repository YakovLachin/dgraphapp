package decision

import (
	"fmt"

	"gitlab.com/YakovLachin/dgraphapp/service"

	google_protobuf "github.com/golang/protobuf/ptypes/struct"
	"github.com/sirupsen/logrus"
)

func GetChatResponseByTreeAndData(tree *service.Node, facts *google_protobuf.Struct) (*service.ChatResponse, error) {
	log := logrus.New()
	log.Level = logrus.DebugLevel
	logger := log.WithField("chanel", "dgraph")
	logger.Info("Start decision" + tree.Name)
	defer logger.Info("Finish decision" + tree.Name)

	jsonFacts, err := facts.MarshalJSON()
	if err != nil {
		return nil, fmt.Errorf("fail facts.MarshalJSON : %s", err.Error())
	}
	logger.Infof("res ---->: start ")

	res := &service.ChatResponse{}
	curNode := tree
	for curNode != nil {
		switch curNode.Type {
		case text:
			res.Events = append(res.Events, &service.Event{
				Type: text,
				Params: []*service.Params{
					{
						Text: curNode.Expression,
					},
				},
			})

			curNode = curNode.Next
		case button:
			curNode = curNode.Next
		case predicate:
			decision, err := getDecisionByNodeAndData(curNode, string(jsonFacts))
			logger.Infof("res ---->: %b, %s ", decision, curNode.Expression)

			if err != nil {
				return nil, fmt.Errorf("fail getDecisionByNodeAndData %s", err.Error())
			}

			if decision == true {
				logger.Info(tree.Name + " : " + string(jsonFacts) + " : " + curNode.Expression + " : " + "true")
				curNode = curNode.Next
			} else {
				logger.Info(tree.Name + " : " + string(jsonFacts) + " : " + curNode.Expression + " : " + "false")
				curNode = curNode.Alternative
			}
		case action:
			curNode = curNode.Next
		case collection:
			btns := make([]*service.Button, 0, len(curNode.Data))
			for _, btn := range curNode.Data {
				fmt.Println( "===============>", btn.Type)
				fmt.Println( "===============>", btn.Expression)
				if btn.Type == button {
					btns = append(btns, &service.Button{
						Expression: btn.Expression,
						Uid:        btn.Uid,
					})
				}
			}

			res.Events = append(res.Events, &service.Event{
				Type: EventButtons,
				Params: []*service.Params{
					{
						Buttons: &service.Buttons{
							Data: btns,
						},
					},
				},
			})

			curNode = curNode.Next
		case EventEnd:
			res.Events = append(res.Events, &service.Event{
				Type: EventEnd,
			})
			return res, nil
		case EventEndSilent:
			res.Events = append(res.Events, &service.Event{
				Type: EventEndSilent,
			})
			return res, nil
		case EventTerminate:
			res.Events = append(res.Events, &service.Event{
				Type: EventTerminate,
			})
			return res, nil
		default:
			return nil, fmt.Errorf("unexpected link node type %s", curNode.Type)
		}
	}

	return res, err
}

package decision

const text             = "text"
const button           = "button"
const collection       = "collection"

const EventText        = "text"
const EventEnd         = "end"
const EventEndSilent   = "end_silent"
const EventTerminate   = "terminate"
const EventButtons     = "buttons"

const action           = "action"
const predicate        = "predicate"

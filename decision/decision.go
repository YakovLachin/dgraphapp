package decision

import (
	"fmt"

	"gitlab.com/YakovLachin/dgraphapp/interpreter"
	"gitlab.com/YakovLachin/dgraphapp/service"

	google_protobuf "github.com/golang/protobuf/ptypes/struct"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

)

func GetDecisionByTreeAndData(tree *service.Node, facts *google_protobuf.Struct) (bool, error) {
	log := logrus.New()
	log.Level = logrus.DebugLevel
	logger := log.WithField("chanel", "dgraph")
	logger.Info("Start decision" + tree.Name)
	defer logger.Info("Finish decision" + tree.Name)

	//pbmarshaler := jsonpb.Marshaler{}
	jsonFacts, err := facts.MarshalJSON()
	if err != nil {
		return false, fmt.Errorf("fail facts.MarshalJSON : %s", err.Error())
	}
	logger.Infof("res ---->: start ")

	curNode := tree
	for isNotLeaf(curNode) {
		if curNode.Type != predicate {
			return false, fmt.Errorf("node %s is not predicate", curNode.Uid)
		}

		decision, err := getDecisionByNodeAndData(curNode, string(jsonFacts))
		logger.Infof("res ---->: %b, %s ", decision, curNode.Expression)

		if err != nil {
			return false, fmt.Errorf("fail getDecisionByNodeAndData %s", err.Error())
		}

		if decision == true {
			logger.Info(tree.Name + " : " + string(jsonFacts) + " : " + curNode.Expression + " : " + "true")
			curNode = curNode.Next
		} else {
			logger.Info(tree.Name + " : " + string(jsonFacts) + " : " + curNode.Expression + " : " + "false")
			curNode = curNode.Alternative
		}
	}

	if curNode.Type != predicate {
		return false, fmt.Errorf("node %s is not predicate", curNode.Uid)
	}

	res, err := getDecisionByNodeAndData(curNode, string(jsonFacts))

	return res, err
}

func isNotLeaf(curNode *service.Node) bool {
	return len(curNode.Data) > 0 || curNode.Alternative != nil || curNode.Next != nil
}

func getDecisionByNodeAndData(node *service.Node, data string) (bool, error) {
	if node == nil || node.Expression == "" {
		return false, errors.New("node is NIl")
	}

	return interpreter.InterpretLuaBool(data, node.Expression)
}

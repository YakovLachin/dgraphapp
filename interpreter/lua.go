package interpreter

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/yuin/gopher-lua"
	luajson "layeh.com/gopher-json"
)

func InterpretLuaBool(jsn string, expr string) (bool, error) {
	L := lua.NewState()
	defer L.Close()
	L.PreloadModule("JSON", luajson.Loader)

	luaExpr := getLuaExpressionString(jsn, expr)
	if err := L.DoString(luaExpr); err != nil {
		return false, fmt.Errorf("fail intepret expression(%s): %s: %s", expr, jsn, err.Error())
	}

	lv := L.Get(-1)
	if res, ok := lv.(lua.LBool); ok {
		// lv is LString
		return toBool(res)

	}

	return false, fmt.Errorf("Fail, interpret expression (%s). Can`t get LBool from Stack After Interpret.", expr)
}

func toBool(lBool lua.LBool) (bool, error) {
	if lBool.String() == "false" {
		return false, nil
	} else if lBool.String() == "true" {
		return true, nil
	} else {
		return false, errors.New("fail get result DAta from lua.LBool")
	}
}

func getLuaExpressionString(jsn string, nodeCode string) string {
	template := `
		local j = require("JSON")
		d = j.decode('%s')
		return `

	hiddenCode := fmt.Sprintf(template, jsn)
	return hiddenCode + nodeCode
}
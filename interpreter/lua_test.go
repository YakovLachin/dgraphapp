package interpreter

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestInterpretLuaBool_EmptyData_Positive(t *testing.T) {
	expected, err := InterpretLuaBool(`{}`, `d.age ~= nil and d.age > 20`)
	assert.NoError(t, err)
	assert.True(t, expected)
}

func TestInterpretLuaBool_NotEmptyData(t *testing.T) {
	expected, err := InterpretLuaBool(`{"age": 21}`, `d.age > 20`)
	assert.NoError(t, err)
	assert.True(t, expected)
}
